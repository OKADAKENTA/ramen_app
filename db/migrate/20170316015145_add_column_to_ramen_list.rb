class AddColumnToRamenList < ActiveRecord::Migration[5.0]
  def change
    add_column :ramen_lists, :shop_name_furi, :string, after: :shop_name
    add_column :ramen_lists, :prefecture, :string , after: :shop_name_furi
    add_column :ramen_lists, :tel, :string
    add_column :ramen_lists, :business_hours, :string
    add_column :ramen_lists, :regular_holiday, :string
    add_column :ramen_lists, :parking_area, :string
    add_column :ramen_lists, :access, :text
    add_column :ramen_lists, :other, :text

  end
end
