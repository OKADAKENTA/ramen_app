class CreateReviewLists < ActiveRecord::Migration[5.0]
  def change
    create_table :review_lists do |t|
      t.references :ramen_list, foreign_key: true
      t.string :eaten_ramen
      t.integer :price
      t.integer :points
      t.text :review


      t.timestamps
    end
    add_index :review_lists, [:ramen_list_id, :created_at]
  end
end
