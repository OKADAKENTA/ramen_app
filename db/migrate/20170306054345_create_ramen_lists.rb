class CreateRamenLists < ActiveRecord::Migration
  def change
    create_table :ramen_lists do |t|
      t.string :shop_name
      t.string :place

      t.timestamps null: false
    end
  end
end
