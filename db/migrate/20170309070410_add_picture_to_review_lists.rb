class AddPictureToReviewLists < ActiveRecord::Migration[5.0]
  def change
    add_column :review_lists, :picture, :string
  end
end
