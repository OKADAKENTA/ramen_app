class AddColumnToReviewList < ActiveRecord::Migration[5.0]
  def change
    add_column :review_lists, :visited, :date, :after => :points
  end
end
