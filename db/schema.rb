# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170323084300) do

  create_table "ramen_lists", force: :cascade do |t|
    t.string   "shop_name"
    t.string   "place"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "shop_name_furi"
    t.string   "prefecture"
    t.string   "tel"
    t.string   "business_hours"
    t.string   "regular_holiday"
    t.string   "parking_area"
    t.text     "access"
    t.text     "other"
  end

  create_table "review_lists", force: :cascade do |t|
    t.integer  "ramen_list_id"
    t.string   "eaten_ramen"
    t.integer  "price"
    t.integer  "points"
    t.text     "review"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "picture"
    t.date     "visited"
    t.index ["ramen_list_id", "created_at"], name: "index_review_lists_on_ramen_list_id_and_created_at"
    t.index ["ramen_list_id"], name: "index_review_lists_on_ramen_list_id"
  end

end
