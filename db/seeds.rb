#ラーメン屋
#1
RamenList.create!(shop_name:  "ラーメン屋　サンプル1号店",
             shop_name_furi: "ラーメンヤ　サンプルイチゴウテン",
             prefecture: "東京都",
             place: "渋谷区",
             tel: Faker::PhoneNumber.phone_number,
             business_hours: "11:00~15:00",
             regular_holiday: "水曜日",
             parking_area: "無",
             access: "「渋谷駅」から徒歩5分",
             other: "")
#2
RamenList.create!(shop_name:  "ラーメン屋　サンプル2号店",
             shop_name_furi: "ラーメンヤ　サンプルニゴウテン",
             prefecture: "神奈川県",
             place: "横浜市神奈川区",
             tel: Faker::PhoneNumber.phone_number,
             business_hours: "11:00~23:00",
             regular_holiday: "年中無休",
             parking_area: "有、10台。",
             access: "「東神奈川」から徒歩2分",
             other: "")
#3             
RamenList.create!(shop_name:  "ラーメン屋　サンプル3号店",
             shop_name_furi: "ラーメンヤ　サンプルサンゴウテン",
             prefecture: "千葉県",
             place: "成田市",
             tel: Faker::PhoneNumber.phone_number,
             business_hours: "11:30~25:00",
             regular_holiday: "火曜日",
             parking_area: "有、20台。",
             access: "車で「成田インターチェンジ」から10分",
             other: "")
#4
RamenList.create!(shop_name:  "ラーメン屋　サンプル4号店",
             shop_name_furi: "ラーメンヤ　サンプルヨンゴウテン",
             prefecture: "栃木県",
             place: "宇都宮市",
             tel: Faker::PhoneNumber.phone_number,
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")
#5
RamenList.create!(shop_name:  "ラーメン屋　サンプル5号店",
             shop_name_furi: "ラーメンヤ　サンプルゴゴウテン",
             prefecture: "群馬県",
             place: "前橋市",
             tel: Faker::PhoneNumber.phone_number,
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")
#6
RamenList.create!(shop_name:  "ラーメン屋　サンプル6号店",
             shop_name_furi: "ラーメンヤ　サンプルロクゴウテン",
             prefecture: "茨城県",
             place: "水戸市",
             tel: Faker::PhoneNumber.phone_number,
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")
#7
RamenList.create!(shop_name:  "ラーメン屋　サンプル7号店",
             shop_name_furi: "ラーメンヤ　サンプルナナゴウテン",
             prefecture: "埼玉県",
             place: "さいたま市",
             tel: Faker::PhoneNumber.phone_number,
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")
#8
RamenList.create!(shop_name:  "ラーメン屋　サンプル8号店",
             shop_name_furi: "ラーメンヤ　サンプルハチゴウテン",
             prefecture: "北海道",
             place: "網走市",
             tel: Faker::PhoneNumber.phone_number,
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")
#9
RamenList.create!(shop_name:  "ラーメン屋　サンプル9号店",
             shop_name_furi: "ラーメンヤ　サンプルキュウゴウテン",
             prefecture: "大阪府",
             place: "堺市",
             tel: Faker::PhoneNumber.phone_number,
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")
#10
RamenList.create!(shop_name:  "ラーメン屋　サンプル10号店",
             shop_name_furi: "ラーメンヤ　サンプルジュウゴウテン",
             prefecture: "福岡県",
             place: "福岡市博多区",
             tel: Faker::PhoneNumber.phone_number,
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")
#11
RamenList.create!(shop_name:  "名前のないラーメン屋",
             shop_name_furi: "ナマエノナイラーメンヤ",
             prefecture: "京都府",
             place: "京都市",
             tel: "",
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")
#12
RamenList.create!(shop_name:  "つじ道ラーメン",
             shop_name_furi: "ツジドウラーメン",
             prefecture: "神奈川県",
             place: "藤沢市",
             tel: "",
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")  
#13
RamenList.create!(shop_name:  "中華蕎麦 とみ田",
             shop_name_furi: "チュウカソバ　トミタ",
             prefecture: "千葉県",
             place: "松戸市",
             tel: "047-368-8860",
             business_hours: "11:00～15:00 （ラストオーダー）",
             regular_holiday: "水曜日（祝日、祭日もお休みです）",
             parking_area: "駐車場なし 付近にコインパーキング多数アリ",
             access: "駅東口（セブン&アイ側）より【マクドナルド前の十字路】を右折（一方通行）を直進150mほど。白提灯に「とみ田」",
             other: "☆ご注意: 店内、厨房内は撮影禁止です。ラーメンの撮影はOKですが、フラッシュと、立ち上がっての撮影だけは禁止となっております。")   
#14
RamenList.create!(shop_name:  "一蘭　町田店",
             shop_name_furi: "イチラン　マチダテン",
             prefecture: "東京都",
             place: "町田市",
             tel: "",
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")  
#15
RamenList.create!(shop_name:  "一蘭　渋谷店",
             shop_name_furi: "イチラン　シブヤテン",
             prefecture: "東京都",
             place: "渋谷区",
             tel: "",
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")  
#16
RamenList.create!(shop_name:  "ど・みそ　町田店",
             shop_name_furi: "ドミソ　マチダテン",
             prefecture: "東京都",
             place: "町田市",
             tel: "",
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")  
#17
RamenList.create!(shop_name:  "ゴル麺。　町田店",
             shop_name_furi: "ゴルメン　マチダテン",
             prefecture: "東京都",
             place: "町田市",
             tel: "",
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")  
#18
RamenList.create!(shop_name:  "博多一風堂　池袋店",
             shop_name_furi: "ハカタイップウドウ　イケブクロテン",
             prefecture: "東京都",
             place: "豊島区",
             tel: "",
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")        
#19
RamenList.create!(shop_name:  "博多一風堂　恵比寿店",
             shop_name_furi: "ハカタイップウドウ　エビステン",
             prefecture: "東京都",
             place: "渋谷区",
             tel: "",
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")     
#20
RamenList.create!(shop_name:  "天下一品　八王子店",
             shop_name_furi: "テンカイッピン　ハチオウジテン",
             prefecture: "東京都",
             place: "八王子市",
             tel: "",
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")    
#21
RamenList.create!(shop_name:  "天下一品　吉祥寺店",
             shop_name_furi: "テンカイッピン　キチジョウジテン",
             prefecture: "東京都",
             place: "武蔵野市",
             tel: "",
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")    
#22
RamenList.create!(shop_name:  "天下一品　関内店",
             shop_name_furi: "テンカイッピン　カンナイテン",
             prefecture: "神奈川県",
             place: "横浜市中区",
             tel: "",
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "") 
#23
RamenList.create!(shop_name:  "天下一品　上野アメ横店",
             shop_name_furi: "テンカイッピン　ウエノアメヨコテン",
             prefecture: "東京都",
             place: "台東区",
             tel: "",
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")  
#24
RamenList.create!(shop_name:  "ラーメン二郎　三田本店",
             shop_name_furi: "ラーメンジロウ　ミタホンテン",
             prefecture: "東京都",
             place: "港区",
             tel: "",
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")  
#25
RamenList.create!(shop_name:  "ラーメン二郎　目黒店",
             shop_name_furi: "ラーメンジロウ　メグロテン",
             prefecture: "東京都",
             place: "目黒区",
             tel: "",
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")  
#26
RamenList.create!(shop_name:  "ラーメン二郎　亀戸店",
             shop_name_furi: "ラーメンジロウ　カメイドテン",
             prefecture: "東京都",
             place: "江東区",
             tel: "",
             business_hours: "",
             regular_holiday: "",
             parking_area: "",
             access: "",
             other: "")  

#サンプルレビュー
ramen_lists = RamenList.order(:created_at).take(10)
eaten_ramens=["醤油ラーメン","味噌ラーメン","豚骨ラーメン","つけ麺","塩ラーメン"]
reviews=["うまかった(/・ω・)/","味が濃かった( ;∀;)","まずかった|дﾟ)","満足でした(^^)/","(´・ω・`)"]
40.times do
  ramen_lists.each { |ramen_list| ramen_list.review_lists.create!(eaten_ramen: eaten_ramens.sample, price: Random.new().rand(3)+1, points: Random.new().rand(100)+1, review: reviews.sample, picture: "", visited: Date.today) }
end

