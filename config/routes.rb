Rails.application.routes.draw do
  #get 'static_pages/home'
  root 'static_pages#home'
  get '/about', to:'static_pages#about'
  get '/ramen_registration',  to:'ramen_lists#new'
  post '/ramen_registration',  to: 'ramen_lists#create'
  get '/review_post',  to:'review_lists#new'
  post '/review_post',  to: 'review_lists#create'
  resources :ramen_lists
  resources :review_lists
end
