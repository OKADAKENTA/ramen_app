# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Rails.application.initialize!

WillPaginate::ViewHelpers.pagination_options[:previous_label] = '&lt 前へ'
WillPaginate::ViewHelpers.pagination_options[:next_label] = '次へ &gt'

custom_mapping_path = "path/mapping_data.yml"
JpPrefecture.setup do |config|
  config.mapping_data = YAML.load_file custom_mapping_path
end

Faker::Config.locale = :ja