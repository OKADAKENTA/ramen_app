# りーどみー(README)  
## アプリファイルへの移動  
`$ cd ramen_app/`  
## bundleのインストール
`$ bundle install`  
## rails serverの起動  
`$ rails server -b $IP -p $PORT`  
## DBのリセットとマイグレート  
`$ rails db:migrate:reset`  
## サンプルデータの入力  
`$ rails db:seed`    
使用可能となります。