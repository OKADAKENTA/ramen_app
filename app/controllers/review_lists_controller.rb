class ReviewListsController < ApplicationController
  
  def new
    @review_list = ReviewList.new
  end
  
  def create
    @review_list = ReviewList.new(review_params)
    @post_ramen_id = params[:review_list][:ramen_list_id]
    if @review_list.save
      flash[:success] = "レビューが投稿されました"
      redirect_to ramen_list_path(@review_list.ramen_list_id)
    else
      render 'new'
    end
  end
  
  private
    
    def review_params
      params.require(:review_list).permit(:ramen_list_id, :eaten_ramen, :price, :points, :visited, :review, :picture)
    end
end
