class RamenListsController < ApplicationController
  
  def index
  #  @ramen_lists = if params[:search]
  #    RamenList.paginate(page: params[:page]).where('shop_name LIKE ?', "%#{params[:search]}%")
  #  else
  #    @ramen_lists= RamenList.paginate(page: params[:page],:per_page => 10)
  #  end
    @ordertype = params[:ordertype]
    #debugger
    if params[:prefecture].nil?
      @pref = ""
    else
      @pref = params[:prefecture]
    end
    @ramen_lists = RamenList.paginate(page: params[:page], :per_page => 10).search(params[:search],params[:ordertype],@pref)
  end
  
  def show
    @ramen_list = RamenList.find(params[:id])
    @review_lists = @ramen_list.review_lists.paginate(page: params[:page], :per_page => 10)
    @review_point_avg = @ramen_list.review_lists.average(:points)
  end
  
  def new
    @ramen_list= RamenList.new
  end
  
  def create
    @ramen_list = RamenList.new(ramen_params)
    if @ramen_list.save
      flash[:info] = "お店を登録しました！"
      redirect_to ramen_list_path(@ramen_list.id)
    else
      render 'new'
    end  
  end

  def edit
    @ramen_list = RamenList.find(params[:id])
  end
  
  def update
        @ramen_list = RamenList.find(params[:id])
    if @ramen_list.update_attributes(ramen_params)
      flash[:success] = "お店の情報を更新しました！"
      redirect_to @ramen_list
    else
      render 'edit'
    end
  end

  private
  
    def ramen_params
      params.require(:ramen_list).permit(:shop_name, :shop_name_furi,:prefecture, :place, :tel, :business_hours,
       :regular_holiday, :parking_area, :access, :other)
    end
end
