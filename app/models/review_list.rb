class ReviewList < ApplicationRecord
  belongs_to :ramen_list
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates :ramen_list_id, presence: true
  validates :eaten_ramen, presence: true
  validates :price, presence: true
  validates :points, presence: true
  validates :visited, presence: true
  validates :review, presence: true

end
