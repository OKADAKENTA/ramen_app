class RamenList < ActiveRecord::Base
  has_many :review_lists, dependent: :destroy
  validates :shop_name,  presence:true
  validates :shop_name_furi,  presence:true, format: { with: /\A[\p{katakana}　ー－&&[^ -~｡-ﾟ]]+\z/ , message: 'は全角カタカナで入力して下さい'}
  validates :prefecture, presence: true
  validates :place, presence: true
  validates :tel, length: {allow_blank:true, in: 12..13}, format: { with: /\A(\A\d{2,5}-\d{1,4}-\d{4}+\z)?\z/ , message: 'は不正な値です。ハイフン付きの半角数字で入力してください' }
  
  def self.search(search,ordertype,pref) 
    # if search
    #   if pref =="全国"
    #     if ordertype == "created_at"
    #       where(['shop_name LIKE ?', "%#{search}%"]).order('created_at desc')
    #     elsif ordertype == "avg-points"
    #       where(['shop_name LIKE ?', "%#{search}%"]).left_joins(:review_lists).group(:id).order('avg(points) desc,shop_name_furi')
    #     elsif ordertype == "aiueo"
    #       where(['shop_name LIKE ?', "%#{search}%"]).order('shop_name_furi')
    #     elsif ordertype == "review-count"
    #       where(['shop_name LIKE ?', "%#{search}%"]).left_joins(:review_lists).group(:id).order('count(review) desc,shop_name_furi')
    #     else
    #       where(['shop_name LIKE ?', "%#{search}%"]).order('created_at desc')
    #     end
    #   else
    #     if ordertype == "created_at"
    #       where(['shop_name LIKE ?', "%#{search}% "]).where(['place LIKE ?', "%#{pref}%"]).order('created_at desc')
    #     elsif ordertype == "avg-points"
    #       where(['shop_name LIKE ?', "%#{search}%"]).where(['place LIKE ?', "%#{pref}%"]).left_joins(:review_lists).group(:id).order('avg(points) desc,shop_name_furi')
    #     elsif ordertype == "aiueo"
    #       where(['shop_name LIKE ?', "%#{search}%"]).where(['place LIKE ?', "%#{pref}%"]).order('shop_name_furi')
    #     elsif ordertype == "review-count"
    #       where(['shop_name LIKE ?', "%#{search}%"]).where(['place LIKE ?', "%#{pref}%"]).left_joins(:review_lists).group(:id).order('count(review) desc,shop_name_furi')
    #     else
    #       where(['shop_name LIKE ?', "%#{search}%"]).where(['place LIKE ?', "%#{pref}%"]).order('created_at desc')
    #     end
    #   end

    if search
     # debugger
      if pref =="全国" || pref == ""
        @search=where(['shop_name LIKE ?', "%#{search}%"])
      else
        @search=where(['shop_name LIKE ?', "%#{search}%"]).where(['prefecture LIKE ?', "%#{pref}%"])
      end
      if ENV["RACK_ENV"] == "production"
        if ordertype == "created_at"
          @search.order('created_at desc')
        elsif ordertype == "avg-points"
          @search.left_joins(:review_lists).group(:id).order('avg(points) desc NULLS LAST,shop_name_furi COLLATE "ja_JP.utf8" asc')
        elsif ordertype == "aiueo"
          @search.order('shop_name_furi COLLATE "ja_JP.utf8" asc')
        elsif ordertype == "review-count"
          @search.left_joins(:review_lists).group(:id).order('count(review) desc,shop_name_furi COLLATE "ja_JP.utf8" asc')
        else
          @search.order('created_at desc')
        end
      else
        if ordertype == "created_at"
          @search.order('created_at desc')
        elsif ordertype == "avg-points"
          @search.left_joins(:review_lists).group(:id).order('avg(points) desc,shop_name_furi')
        elsif ordertype == "aiueo"
          @search.order('shop_name_furi')
        elsif ordertype == "review-count"
          @search.left_joins(:review_lists).group(:id).order('count(review) desc,shop_name_furi')
        else
          @search.order('created_at desc')
        end
      end
    else
      #all
      if ENV["RACK_ENV"] == "production"
        if ordertype == "created_at"
          all.order('created_at desc')
        elsif ordertype == "avg-points"
          all.left_joins(:review_lists).group(:id).order('avg(points) desc NULLS LAST,shop_name_furi COLLATE "ja_JP.utf8" asc')
          #joins(:review_lists).group(:ramen_list_id).order('points desc').average(:points)
        elsif ordertype == "aiueo"
          all.order('shop_name_furi COLLATE "ja_JP.utf8" asc')
        elsif ordertype == "review-count"
          all.left_joins(:review_lists).group(:id).order('count(review) desc,shop_name_furi COLLATE "ja_JP.utf8" asc')
        else
          all.order('created_at desc')
        end
      else
        if ordertype == "created_at"
          all.order('created_at desc')
        elsif ordertype == "avg-points"
          all.left_joins(:review_lists).group(:id).order('avg(points) desc,shop_name_furi')
          #joins(:review_lists).group(:ramen_list_id).order('points desc').average(:points)
        elsif ordertype == "aiueo"
          all.order('shop_name_furi')
        elsif ordertype == "review-count"
          all.left_joins(:review_lists).group(:id).order('count(review) desc,shop_name_furi')
        else
          all.order('created_at desc')
        end
      end
    end
  end
end
