module ReviewListsHelper
  
  def price_conversion(price)
    if price==1 then
      price_text="安い";  
    elsif price==2 then
      price_text="普通";      
    elsif price==3 then
      price_text="高い";    
    end

  end
  
  def ramen_id_selected(shop_id)
    if shop_id == nil then
      
      if @post_ramen_id == nil then
        ""
      else

        @post_ramen_id

      end
    else
      find_id = RamenList.find_by(id: shop_id)

      if find_id == nil then
        ""
      else
        shop_id
      end
    end
  end
  
end
