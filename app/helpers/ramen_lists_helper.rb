module RamenListsHelper
  def now_sort(sort)
    if params[:ordertype] == nil && sort=="created_at"
      false
    elsif sort=="created_at" && params[:ordertype] == ""
      false
    else
      sort != params[:ordertype]
    end
  end
  
  def list_count(search,pref)
    if search == nil && pref == ""
      RamenList.all.count
    elsif pref == "全国"
      RamenList.where(['shop_name LIKE ?', "%#{search}%"]).count
    else
      RamenList.where(['shop_name LIKE ?', "%#{search}%"]).where(['prefecture LIKE ?', "%#{pref}%"]).count
    end
  end
end
